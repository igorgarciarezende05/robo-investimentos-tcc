import numpy as np
np.random.seed(1)

import math
import MetaTrader5 as mt5
import pandas as pd
import matplotlib.pyplot as plt
import time
import pytz
from datetime import datetime
import dateutil.relativedelta
from keras.models import Sequential
from keras.layers.core import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
plt.style.use('fivethirtyeight')


mt5.initialize()

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1500)

timezone = pytz.timezone("ETC/UTC")

date_ano = datetime.today() + dateutil.relativedelta.relativedelta(months=-12)
date_ano_passado = date_ano.strftime('%Y-%m-%d')
date_ap_pregao = date_ano_passado + '-09:00:00'

diff_inicio_pregao = datetime.today() - pd.Timestamp(date_ap_pregao)

minutos = 5

quant_barras = int(diff_inicio_pregao.total_seconds()/(minutos*60))

ativo = 'WDO$'

barras = mt5.copy_rates_from(ativo, mt5.TIMEFRAME_M5, datetime.today(), quant_barras)
barras_frame = pd.DataFrame(barras)
barras_frame['time'] = pd.to_datetime(barras_frame['time'], unit='s')

barras_total = barras_frame[barras_frame['time'] >= date_ap_pregao].reset_index(drop = True)

plt.figure(figsize=(16,8))
plt.title('Close Price History')
plt.plot(barras_total['time'], barras_total['close'], linewidth=1)
plt.xlabel('Date', fontsize=18)
plt.ylabel('Close Price', fontsize=18)
plt.show()

data = barras_total.filter(['close'])
dataset = data.values
training_data_len = math.ceil( len(dataset) * .8 )

scaler = MinMaxScaler(feature_range=(0,1))
scaled_data = scaler.fit_transform(dataset)

train_data = scaled_data[0:training_data_len , :]

x_train = [] # independent training variables or trainig features
y_train = [] # deep variables or target variables

for i in range(60, len(train_data)):
    x_train.append(train_data[i-60:i, 0])
    y_train.append(train_data[i, 0])

#Convert the x_train and y_train into numpy arrays
x_train, y_train = np.array(x_train), np.array(y_train)

#Reshape the data
x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))

#Build LSTM Model
model = Sequential()
model.add(LSTM(50, return_sequences=True, input_shape=(x_train.shape[1], 1)))
# 50 neurons
# return_sequence = true because we'll be using another LSTM layer
# input_shape(number of timesteps, number of features)
model.add(LSTM(50, return_sequences=False))
model.add(Dense(25))
model.add(Dense(1))

#Compile the model
model.compile(optimizer='adam', loss='mean_squared_error')
# optimizer = used to improve upon the loss function
# loss function = used to measure how well the model did on training

model.fit(x_train, y_train, batch_size=1, epochs=1)

#Create the test dataset
#Create a new Array containing scaled values
test_data = scaled_data[training_data_len - 60: , :]

#Create x_test and y_test
x_test = []
y_test = dataset[training_data_len, :]
for i in range(60, len(test_data)):
    x_test.append(test_data[i-60:i, 0])

#Convert to numpy array so it can be used on the LSTM model
x_test = np.array(x_test)

#Reshape the data (data is 2-dimensional but model expects 3-dimensional)
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))  #Number of rows(samples), number of collumns(timesteps), features(close price)

#Get the models predicted values
predictions = model.predict(x_test)
predictions = scaler.inverse_transform(predictions)

#Get the root mean squared error (RMSE)
rmse = np.sqrt( np.mean(predictions - y_test)**2 )

#Plot the data
train = data[:training_data_len]
valid = data[training_data_len:]
valid['predictions'] = predictions
#Vizualize the data
plt.figure(figsize=(50,25))
plt.title('Model')
plt.xlabel('Date', fontsize=18)
plt.ylabel('Close Price', fontsize=18)
plt.plot(train['close'], linewidth=1)
plt.plot(valid[['close', 'predictions']], linewidth=1)
plt.legend(['Train', 'Validation', 'Prediction'], loc='lower right')
plt.show()

#Show valid and predicted prices
print(valid)
print(rmse)


#Create new DataFrame
new_df = barras_total['close']
#Get the last 60 days of data and convert to an array
last_60_days = new_df[-60:].values
last_60_days = last_60_days.reshape(-1, 1)
#Scale data to values between 0 and 1
last_60_days_scaled = scaler.transform(last_60_days)

X_test = []
X_test.append(last_60_days_scaled)
X_test = np.array(X_test)
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

pred_price = model.predict(X_test)
pred_price = scaler.inverse_transform(pred_price)

print(pred_price)

time.sleep(300)
barras = mt5.copy_rates_from(ativo, mt5.TIMEFRAME_M5, datetime.today(), 1)
barras_frame = pd.DataFrame(barras)
barras_frame['time'] = pd.to_datetime(barras_frame['time'], unit='s')

barras_total = barras_frame[barras_frame['time'] >= date_ap_pregao].reset_index(drop = True)
print(barras_total)